package com.lviv.hv.accounting.controller;

import com.lviv.hv.accounting.domain.PurchaseCategory;
import com.lviv.hv.accounting.service.PurchaseCategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PurchaseCategoryController {

  @Autowired
  private PurchaseCategoryService purchaseCategoryService;

  @RequestMapping(value = "/api/purchaseCategory/{purchaseCategoryId}", method = RequestMethod.GET)
  public ResponseEntity<PurchaseCategory> getPurchaseCategoryById(@PathVariable Long purchaseCategoryId) {
    PurchaseCategory purchaseCategory = purchaseCategoryService.findById(purchaseCategoryId);
    return new ResponseEntity<>(purchaseCategory, HttpStatus.OK);
  }

  @RequestMapping(value = "/api/purchaseCategory", method = RequestMethod.GET)
  public ResponseEntity<List<PurchaseCategory>> getPurchaseCategories() {
    List<PurchaseCategory> purchaseCategories = purchaseCategoryService.findAll();
    return new ResponseEntity<>(purchaseCategories, HttpStatus.OK);
  }

  @RequestMapping(value = "/api/purchaseCategory", method = RequestMethod.POST)
  public ResponseEntity<PurchaseCategory> addPurchaseCategory(@RequestBody PurchaseCategory purchaseCategory){
    purchaseCategoryService.create(purchaseCategory);
    return new ResponseEntity<>(purchaseCategory, HttpStatus.CREATED);
  }

  @RequestMapping(value = "/api/purchaseCategory/{purchaseCategoryId}", method = RequestMethod.PUT)
  public ResponseEntity<PurchaseCategory> updatePurchaseCategory(@PathVariable Long purchaseCategoryId,
      @RequestBody PurchaseCategory updatePurchaseCategory){
    PurchaseCategory purchaseCategory = purchaseCategoryService.findById(purchaseCategoryId);
    purchaseCategory.setName(updatePurchaseCategory.getName());
    purchaseCategory.setDescription(updatePurchaseCategory.getDescription());
    purchaseCategory.setPurchases(updatePurchaseCategory.getPurchases());
    purchaseCategoryService.update(purchaseCategory);
    return new ResponseEntity<>(purchaseCategory , HttpStatus.CREATED);
  }

  @RequestMapping(value = "/api/purchaseCategory/{purchaseCategoryId}", method = RequestMethod.DELETE)
  public ResponseEntity<HttpStatus> deletePurchaseCategory(@PathVariable Long purchaseCategoryId) {
    purchaseCategoryService.delete(purchaseCategoryId);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
