package com.lviv.hv.accounting.controller;

import com.lviv.hv.accounting.domain.Purchase;
import com.lviv.hv.accounting.model.PurchaseModel;
import com.lviv.hv.accounting.service.PurchaseElasticsearchService;
import com.lviv.hv.accounting.service.PurchaseService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ElasticsearchController {

  @Autowired
  PurchaseService purchaseService;

  @Autowired
  PurchaseElasticsearchService purchaseElasticsearchService;

  @Autowired
  ElasticsearchTemplate elasticsearchTemplate;


  @GetMapping(value="/init")
  public String init(Model model){
    elasticsearchTemplate.createIndex(Purchase.class);
    List<Purchase> purchases = purchaseService.getPurchases();
    purchaseElasticsearchService.saveAll(purchases);
    return "***DONE***";
  }

}
