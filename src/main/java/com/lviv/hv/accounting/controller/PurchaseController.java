package com.lviv.hv.accounting.controller;

import com.lviv.hv.accounting.domain.Purchase;
import com.lviv.hv.accounting.domain.UserPurchase;
import com.lviv.hv.accounting.domain.PurchaseCategory;
import com.lviv.hv.accounting.model.PurchaseModel;
import com.lviv.hv.accounting.service.PurchaseCategoryService;
import com.lviv.hv.accounting.service.PurchaseElasticsearchService;
import com.lviv.hv.accounting.service.PurchaseService;

import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PurchaseController {

  @Autowired
  PurchaseService purchaseService;

  @Autowired
  PurchaseCategoryService purchaseCategoryService;

  @Autowired
  PurchaseElasticsearchService purchaseElasticsearchService;

  @RequestMapping(value = "/purchase/{purchaseId}", method = RequestMethod.GET)
  public String getPurchaseById(@PathVariable Long purchaseId) {
    UserPurchase userPurchase = purchaseService.findById(purchaseId);
    return "purchase-view";
  }

  @RequestMapping(value = "/purchase", method = RequestMethod.GET)
  public String getPurchases(HttpServletRequest request, Model model) {
    List<UserPurchase> userPurchases = purchaseService.findAll();
    List<PurchaseCategory> purchaseCategories = purchaseCategoryService.findAll();
    List<Purchase> purchases = purchaseElasticsearchService.findAll();
    model.addAttribute("purchaseNames", purchases);
    model.addAttribute("purchases", userPurchases);
    model.addAttribute("purchaseCategories", purchaseCategories);
    return "purchases-view";
  }

  @RequestMapping(value = "/purchase", method = RequestMethod.POST)
  public String addPurchase(Model model, @ModelAttribute("purchase") PurchaseModel purchaseModel){
    UserPurchase userPurchase = purchaseModel.getPurchase();
    PurchaseCategory purchaseCategory = purchaseCategoryService.findById(purchaseModel.getPurchaseCategoryId());
    userPurchase.setPurchaseCategory(purchaseCategory);
    userPurchase.setDate(LocalDate.now());
    Purchase purchase = purchaseElasticsearchService.getByName(purchaseModel.getName());
    if(purchase == null){
      Purchase newPurchase = new Purchase(purchaseModel.getName(), purchaseCategory);
        purchaseService.addPurchase(newPurchase);
        userPurchase.setPurchase(newPurchase);
    }else{
      userPurchase.setPurchase(purchase);
    }
    purchaseService.addUserPurchase(userPurchase);
    model.addAttribute("purchaseNames", purchaseElasticsearchService.findAll());
    model.addAttribute("purchases", purchaseService.findAll());
    model.addAttribute("purchaseCategories", purchaseCategoryService.findAll());
    return "purchases-view";
  }

//  @RequestMapping(value = "/api/purchase/{purchaseId}", method = RequestMethod.PUT)
//  public ResponseEntity<UserPurchase> updatePurchase(@PathVariable Long purchaseId, @RequestBody UserPurchase updateUserPurchase){
//    UserPurchase userPurchase = purchaseService.findById(purchaseId);
//    userPurchase.setName(updateUserPurchase.getName());
//    userPurchase.setPrice(updateUserPurchase.getPrice());
//    purchaseService.update(userPurchase);
//    return new ResponseEntity<>(userPurchase , HttpStatus.CREATED);
//  }

  @RequestMapping(value = "/api/purchase/{purchaseId}", method = RequestMethod.DELETE)
  public ResponseEntity<HttpStatus> deletePurchase(@PathVariable Long purchaseId) {
    purchaseService.delete(purchaseId);
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
