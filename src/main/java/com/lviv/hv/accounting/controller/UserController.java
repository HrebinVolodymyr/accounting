package com.lviv.hv.accounting.controller;

import com.lviv.hv.accounting.domain.User;
import com.lviv.hv.accounting.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class UserController {

  @Autowired
  private UserService userService;

  @GetMapping(value="/user/add")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String getAddUserForm(Model model){
    return "user-view";
  }

  @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
  public String getUserById(Model model, @PathVariable long userId) {
    User user = userService.findById(userId);
    model.addAttribute("user", user);
    return "user-view";
  }

  @RequestMapping(value = "/user", method = RequestMethod.GET)
  public String getUsers(HttpServletRequest request, Model model) {
    List<User> users = userService.findAll();
    model.addAttribute("users", users);
    return "users-view";
  }

  @RequestMapping(value = "/user", method = RequestMethod.POST)
  public String addUser(Model model, @ModelAttribute("user") User user){
    userService.create(user);
    List<User> users = userService.findAll();
    model.addAttribute("users", users);
    return "users-view";
  }

  @RequestMapping(value = "/user/{userId}", method = RequestMethod.POST)
  public String updateUser(Model model, @PathVariable Long userId, @ModelAttribute("user") User updateUser){
    User user = userService.findById(userId);
    user.setFirstName(updateUser.getFirstName());
    user.setLastName(updateUser.getLastName());
    user.setEmail(updateUser.getEmail());
    userService.update(user);
    List<User> users = userService.findAll();
    model.addAttribute("users", users);
    return "users-view";
  }

  @RequestMapping(value = "/user/{userId}", method = RequestMethod.DELETE)
  public String deleteUser(Model model, @PathVariable Long userId) {
    userService.delete(userId);
    List<User> users = userService.findAll();
    model.addAttribute("users", users);
    return "users-view";
  }
}
