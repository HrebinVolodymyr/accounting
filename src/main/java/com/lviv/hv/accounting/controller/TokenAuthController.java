package com.lviv.hv.accounting.controller;

import com.lviv.hv.accounting.security.UserAuthentication;
import com.lviv.hv.accounting.service.impl.TokenAuthService;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenAuthController {

  @Autowired
  private TokenAuthService tokenAuthService;

  @RequestMapping(value = "/api/authentication", method = RequestMethod.GET)
  private ResponseEntity<UserAuthentication> getAuthentication(HttpServletRequest request) {
    UserAuthentication userAuthentication = tokenAuthService.getAuthentication(request).orElse(null);
    return new ResponseEntity<UserAuthentication>(userAuthentication, HttpStatus.OK);
  }

  @RequestMapping(value = "/api/token", method = RequestMethod.GET)
  public ResponseEntity<String> getToken(HttpServletRequest request) {
    String token = tokenAuthService.getToken(request);
    return new ResponseEntity<>(token, HttpStatus.OK);
  }

}
