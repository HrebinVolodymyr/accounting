package com.lviv.hv.accounting;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.lviv.hv.accounting.repository")
@ComponentScan(basePackages = { "com.lviv.hv.accounting.service" })
public class ElasticsearchConfig {

  @Value("${elasticsearch.cluster.name:docker-cluster}")
  private String clusterName;
  @Value("${elasticsearch.host:192.168.99.100}")
  public String host;
  @Value("${elasticsearch.port:9300}")
  public int port;
  public String getHost() {
    return host;
  }
  public int getPort() {
    return port;
  }

  @Bean
  public Client client() {
    Settings elasticsearchSettings = Settings.builder()
        .put("cluster.name", "docker-cluster").build();
    TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);
    try {
      client.addTransportAddress(new TransportAddress(InetAddress.getByName("elasticsearch"), 9300));
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    return client;
  }

  @Bean
  public ElasticsearchOperations elasticsearchTemplate() {
    return new ElasticsearchTemplate(client());
  }

}
