package com.lviv.hv.accounting.model;

import com.lviv.hv.accounting.domain.UserPurchase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseModel {

    private String id;
    private String name;
    private Long price;
    private Long quantity;
    private LocalDate date;
    private Long purchaseCategoryId;

    public UserPurchase getPurchase(){
        UserPurchase userPurchase = new UserPurchase(price, quantity, date);
        if(this.id != null){
            userPurchase.setId(Long.parseLong(this.id));
        }
        return userPurchase;
    }
}
