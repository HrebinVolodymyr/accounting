package com.lviv.hv.accounting.domain;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPurchase {

  private Long id;
  private Purchase purchase;
  private Long price;
  private Long quantity;
  private LocalDate date;
  private PurchaseCategory purchaseCategory;

  public UserPurchase(Long price, Long quantity, LocalDate date) {
    this.price = price;
    this.quantity = quantity;
    this.date = date;
  }
}
