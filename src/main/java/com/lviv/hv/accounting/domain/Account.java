package com.lviv.hv.accounting.domain;

import java.util.List;
import lombok.Data;

@Data
public class Account {

  private long id;
  private User user;
  private String login;
  private String password;
  private boolean accountNonExpired;
  private boolean accountNonLocked;
  private boolean credentialsNonExpired;
  private boolean enabled;
  private List<Role> authorities;
}
