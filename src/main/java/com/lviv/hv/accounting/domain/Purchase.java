package com.lviv.hv.accounting.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "purchase", type = "purchaseModel")
public class Purchase {
  public Purchase(String name, PurchaseCategory purchaseCategory){
    this.name = name;
    this.purchaseCategory = purchaseCategory;
  }

  @Id
  private Long id;
  private String name;
  private PurchaseCategory purchaseCategory;

}
