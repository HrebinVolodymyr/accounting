package com.lviv.hv.accounting;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@MapperScan("com.lviv.hv.accounting.dao")
public class AccountingApplication {

  public static void main(String[] args) {
    SpringApplication.run(AccountingApplication.class, args);
  }

}
