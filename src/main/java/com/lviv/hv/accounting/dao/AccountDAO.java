package com.lviv.hv.accounting.dao;

import com.lviv.hv.accounting.domain.Account;
import java.util.Optional;

public interface AccountDAO extends GeneralDAO<Account, Long> {
  public Account findByLogin(String login);
}
