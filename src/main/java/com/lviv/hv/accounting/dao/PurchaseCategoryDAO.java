package com.lviv.hv.accounting.dao;

import com.lviv.hv.accounting.domain.PurchaseCategory;

public interface PurchaseCategoryDAO extends GeneralDAO<PurchaseCategory, Long> {

}
