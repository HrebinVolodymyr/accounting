package com.lviv.hv.accounting.dao;

import com.lviv.hv.accounting.domain.Purchase;
import com.lviv.hv.accounting.domain.UserPurchase;
import java.util.List;

public interface PurchaseDAO extends GeneralDAO<UserPurchase, Long> {

  List<UserPurchase> getUserPurchaseByUserId(Long userId);

  List<Purchase> getPurchases();

  void addUserPurchase(UserPurchase userPurchase);

  void addPurchase(Purchase purchase);

}
