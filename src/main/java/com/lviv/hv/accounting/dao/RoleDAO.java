package com.lviv.hv.accounting.dao;

import com.lviv.hv.accounting.domain.Role;

public interface RoleDAO extends GeneralDAO<Role, Long> {
}
