package com.lviv.hv.accounting.dao;

import java.util.List;

public interface GeneralDAO<T, I> {
	
	List<T> findAll();

	T findById(I id);

	Long create(T entity);

	void update(T entity);	
	
	void delete(I id);

}
