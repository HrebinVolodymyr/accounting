package com.lviv.hv.accounting.service.impl;

import com.lviv.hv.accounting.dao.PurchaseDAO;
import com.lviv.hv.accounting.domain.Purchase;
import com.lviv.hv.accounting.domain.UserPurchase;
import com.lviv.hv.accounting.service.PurchaseElasticsearchService;
import com.lviv.hv.accounting.service.PurchaseService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseServiceImpl implements PurchaseService {

  @Autowired
  PurchaseDAO purchaseDAO;

  @Autowired
  PurchaseElasticsearchService purchaseElasticsearchService;

  @Override
  public List<UserPurchase> findAll() {
    return purchaseDAO.findAll();
  }

  @Override
  public UserPurchase findById(Long id) {
    return purchaseDAO.findById(id);
  }

  @Override
  public Long create(UserPurchase entity) {
    return purchaseDAO.create(entity);
  }

  @Override
  public void update(UserPurchase entity) {
    purchaseDAO.update(entity);
  }

  @Override
  public void delete(Long id) {
    purchaseDAO.delete(id);
  }

  @Override
  public List<UserPurchase> getUserPurchaseByUserId(Long userId) {
    return purchaseDAO.getUserPurchaseByUserId(userId);
  }

  @Override
  public void addUserPurchase(UserPurchase purchase) {
    purchaseDAO.addUserPurchase(purchase);
  }

  @Override
  public void addPurchase(Purchase purchase) {
    purchaseDAO.addPurchase(purchase);
    purchaseElasticsearchService.save(purchase);
  }

  @Override
  public List<Purchase> getPurchases(){
    return purchaseDAO.getPurchases();
  }
}
