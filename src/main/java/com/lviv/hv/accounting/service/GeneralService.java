package com.lviv.hv.accounting.service;

import java.util.List;

public interface GeneralService<T, I> {

  List<T> findAll();

  T findById(I id);

  Long create(T entity);

  void update(T entity); 

  void delete(I id);
}
