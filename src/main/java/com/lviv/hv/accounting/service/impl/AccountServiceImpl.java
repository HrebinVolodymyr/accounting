package com.lviv.hv.accounting.service.impl;

import com.lviv.hv.accounting.dao.AccountDAO;
import com.lviv.hv.accounting.domain.Account;
import com.lviv.hv.accounting.service.AccountService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

  @Autowired
  AccountDAO accountDAO;

  @Override
  public List<Account> findAll() {
    return accountDAO.findAll();
  }

  @Override
  public Account findById(Long id) {
    return accountDAO.findById(id);
  }

  @Override
  public Long create(Account entity) {
    return accountDAO.create(entity);
  }

  @Override
  public void update(Account entity) {
    accountDAO.update(entity);
  }

  @Override
  public void delete(Long id) {
    accountDAO.delete(id);
  }

  @Override
  public Optional<Account> findByLogin(String login) {
    return Optional.ofNullable(accountDAO.findByLogin(login));
  }
}
