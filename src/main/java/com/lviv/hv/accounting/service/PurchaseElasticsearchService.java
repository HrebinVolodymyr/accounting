package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.domain.Purchase;
import java.util.List;

public interface PurchaseElasticsearchService extends GeneralService<Purchase, Long> {

  Purchase getByName(String name);

  void save(Purchase entity);

  void saveAll(List<Purchase> purchases);

}
