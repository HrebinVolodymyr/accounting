package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.domain.Account;
import java.util.Optional;

public interface AccountService extends GeneralService<Account, Long> {
  public Optional<Account> findByLogin(String login);
}
