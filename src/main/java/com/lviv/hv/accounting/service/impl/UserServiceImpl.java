package com.lviv.hv.accounting.service.impl;

import java.util.List;

import com.lviv.hv.accounting.dao.UserDAO;
import com.lviv.hv.accounting.domain.User;
import com.lviv.hv.accounting.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO userDAO;

	@Override
	public List<User> findAll(){
		return userDAO.findAll();
	}

	@Override
	public User findById(Long id){
		return userDAO.findById(id);
	}

	@Override
	public Long create(User entity){
		return userDAO.create(entity);
	}

	@Override
	public void update(User entity){
		userDAO.update(entity);		
	}

	@Override
	public void delete(Long id){
		userDAO.delete(id);	
	}

}
