package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.domain.Purchase;
import com.lviv.hv.accounting.domain.UserPurchase;
import java.util.List;

public interface PurchaseService extends GeneralService<UserPurchase, Long>{

  List<UserPurchase> getUserPurchaseByUserId(Long userId);

  List<Purchase> getPurchases();

  void addUserPurchase(UserPurchase userPurchase);

  void addPurchase(Purchase purchase);

}
