package com.lviv.hv.accounting.service.impl;

import java.util.List;

import com.lviv.hv.accounting.dao.RoleDAO;
import com.lviv.hv.accounting.domain.Role;
import com.lviv.hv.accounting.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = "roleCache")
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RoleDAO roleDAO;

	@Override
	public List<Role> findAll(){
		return roleDAO.findAll();
	}

	@Override
	public Role findById(Long id){
		return roleDAO.findById(id);
	}

	@Override
	public Long create(Role entity){
		 return roleDAO.create(entity);
	}	
	
	@Override
	public void update(Role entity){
	    roleDAO.create(entity);		
	}	
	
	@Override
	public void delete(Long id){
		roleDAO.delete(id);	
	}
}
