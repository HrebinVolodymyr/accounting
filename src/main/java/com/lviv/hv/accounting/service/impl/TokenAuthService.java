package com.lviv.hv.accounting.service.impl;

import com.lviv.hv.accounting.domain.Account;
import com.lviv.hv.accounting.security.UserAuthentication;
import com.lviv.hv.accounting.service.AccountService;
import com.lviv.hv.accounting.service.UserService;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class TokenAuthService {

  private static final String AUTH_HEADER_NAME = "X-Auth-Token";

  @Autowired
  private TokenHandler tokenHandler;
  @Autowired
  private AccountService accountService;

  public Optional<UserAuthentication> getAuthentication(HttpServletRequest request) {
    return Optional
        .ofNullable(request.getHeader(AUTH_HEADER_NAME))
        .flatMap(tokenHandler::extractUserLogin)
        .flatMap(accountService::findByLogin)
        .map(UserAuthentication::new);
  }

  public String getToken(HttpServletRequest request){
    String login = request.getHeader("login");
    String password = request.getHeader("password");
    Optional<Account> account = accountService.findByLogin(login);
    if(account.isPresent()&&account.get().getPassword().equals(password)){
      return tokenHandler.generateAccessToken(login, LocalDateTime.now().plusDays(100));
    }
    return "";
  }

}
