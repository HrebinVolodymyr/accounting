package com.lviv.hv.accounting.service.impl;

import com.google.common.collect.Lists;
import com.lviv.hv.accounting.domain.Purchase;
import com.lviv.hv.accounting.repository.PurchaseElasticsearchRepository;
import com.lviv.hv.accounting.service.PurchaseElasticsearchService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseElasticsearchServiceImpl implements PurchaseElasticsearchService {

  @Autowired
  private PurchaseElasticsearchRepository purchaseElasticsearchRepository;

  @Override
  public Purchase getByName(String name) {
    return purchaseElasticsearchRepository.getByName(name);
  }

  @Override
  public void saveAll(List<Purchase> purchases) {
    purchaseElasticsearchRepository.saveAll(purchases);
  }

  @Override
  public List<Purchase> findAll() {
    return Lists.newArrayList(purchaseElasticsearchRepository.findAll());
  }

  @Override
  public Purchase findById(Long id) {
    return purchaseElasticsearchRepository.findById(id).orElse(null);
  }

  @Override
  public Long create(Purchase entity) {
    return null;
  }

  @Override
  public void save(Purchase entity) {
    purchaseElasticsearchRepository.save(entity);
  }

  @Override
  public void update(Purchase entity) {
    purchaseElasticsearchRepository.save(entity);
  }

  @Override
  public void delete(Long id) {
    purchaseElasticsearchRepository.deleteById(id);
  }
}
