package com.lviv.hv.accounting.service.impl;

import com.lviv.hv.accounting.dao.PurchaseCategoryDAO;
import com.lviv.hv.accounting.domain.PurchaseCategory;
import com.lviv.hv.accounting.service.PurchaseCategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseCategoryServiceImpl implements PurchaseCategoryService {

  @Autowired
  PurchaseCategoryDAO purchaseCategoryDAO;

  @Override
  public List<PurchaseCategory> findAll() {
    return purchaseCategoryDAO.findAll();
  }

  @Override
  public PurchaseCategory findById(Long id) {
    return purchaseCategoryDAO.findById(id);
  }

  @Override
  public Long create(PurchaseCategory entity) {
    return purchaseCategoryDAO.create(entity);
  }

  @Override
  public void update(PurchaseCategory entity) {
    purchaseCategoryDAO.update(entity);
  }

  @Override
  public void delete(Long id) {
    purchaseCategoryDAO.delete(id);
  }
}
