package com.lviv.hv.accounting.service.impl;

import com.google.common.io.BaseEncoding;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import lombok.NonNull;
import org.springframework.stereotype.Component;

@Component
public class TokenHandler {

  private final SecretKey secretKey;

  public TokenHandler() {
    String jwtKey = "moernbvsdbwhbfsdvnleakbavls";
    byte[] decodedKey = BaseEncoding.base64().decode(jwtKey);
    secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
  }

  public Optional<String> extractUserLogin(@NonNull String token) {
    try {
      Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
      Claims body = claimsJws.getBody();
      return Optional
          .ofNullable(body.getId());
    } catch (RuntimeException e) {
      return Optional.empty();
    }
  }

  public String generateAccessToken(@NonNull String login, @NonNull LocalDateTime expires) {
    return Jwts.builder()
        .setId(login)
        .setExpiration(Date.from(expires.atZone(ZoneId.systemDefault()).toInstant()))
        .signWith(SignatureAlgorithm.HS512, secretKey)
        .compact();
  }
}
