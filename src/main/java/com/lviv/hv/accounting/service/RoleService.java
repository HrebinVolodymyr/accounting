package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.domain.Role;

public interface RoleService extends GeneralService<Role, Long>{

}
