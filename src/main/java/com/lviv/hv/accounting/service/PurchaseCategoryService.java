package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.domain.PurchaseCategory;

public interface PurchaseCategoryService extends GeneralService<PurchaseCategory, Long> {

}
