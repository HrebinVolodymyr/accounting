package com.lviv.hv.accounting.repository;

import com.lviv.hv.accounting.domain.Purchase;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseElasticsearchRepository extends ElasticsearchRepository<Purchase, Long> {
  Purchase getByName(String name);
}
