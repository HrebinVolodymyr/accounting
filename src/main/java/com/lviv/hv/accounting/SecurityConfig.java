package com.lviv.hv.accounting;

import com.lviv.hv.accounting.filter.TokenAuthFilter;
import com.lviv.hv.accounting.service.impl.TokenAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private TokenAuthService tokenAuthService;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .addFilterBefore(new TokenAuthFilter(tokenAuthService), UsernamePasswordAuthenticationFilter.class)
        .authorizeRequests()
        .antMatchers(
            "/user",
            "/purchase",
            "/user/**",
            "/authentication",
            "/token",
            "/",
            "/init",
                "/purchaseCategory",
                "/user/**",
                "/user",
                "/user/add",
                "/",
                "/index",
                "/login",
                "/logout",
                "/error",
                "/css/**",
                "/js/**",

            "/swagger-ui.html",
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources",
            "/configuration/security",
            "/webjars/**",
            "/h2"
            ).permitAll()
        .anyRequest().authenticated()
        .and()
    .exceptionHandling()
    .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
  }

}
