insert into user
values(1,'Volodymyr', 'Ranga@gmail.com', 0);
insert into user
values(2,'Olena', 'Ravi@gmail.com', 0);

insert into purchase_category
values(1,'Food', 'foodf');
insert into purchase_category
values(2,'Equipment', 'equipment');
insert into purchase_category
values(3,'Stationery', 'stationery');

insert into purchase
values(1,'Toster', 85550, 2);
insert into purchase
values(2,'Pen', 3570, 3);
insert into purchase
values(3,'Bread', 1545, 1);
insert into purchase
values(4,'SSD', 155000, 2);
insert into purchase
values(5,'Handbag', 123000, 2);

insert into user_purchase
values(1,1);
insert into user_purchase
values(1,2);
insert into user_purchase
values(1,3);
insert into user_purchase
values(1,4);
insert into user_purchase
values(2,2);
insert into user_purchase
values(2,3);
insert into user_purchase
values(2,5);

