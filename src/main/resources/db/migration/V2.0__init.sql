CREATE SEQUENCE purchase_id_seq;
CREATE SEQUENCE user_purchase_id_seq;

CREATE TABLE users(
   uid   BIGSERIAL            NOT NULL,
   first_name VARCHAR (20)     NOT NULL,
   last_name VARCHAR (20)     NOT NULL,
   email VARCHAR (200)         NOT NULL,
   account_id INT,
   PRIMARY KEY (uid)
);

CREATE TABLE account(
   aid   BIGSERIAL                  NOT NULL,
   login VARCHAR (20)     NOT NULL,
   password VARCHAR (20)         NOT NULL,
   accountNonExpired BOOLEAN  NOT NULL,
   accountNonLocked BOOLEAN  NOT NULL,
   credentialsNonExpired BOOLEAN  NOT NULL,
   enabled BOOLEAN  NOT NULL,
   user_id INT,
   PRIMARY KEY (aid)
);

CREATE TABLE role(
   rid   BIGSERIAL                  NOT NULL,
   name VARCHAR (20)          NOT NULL,
   PRIMARY KEY (rid)
);

CREATE TABLE account_role(
   account_id   INT         NOT NULL,
   role_id INT       NOT NULL,
   constraint fk_account_id foreign key(account_id) references account(aid),
   constraint fk_role_id foreign key(role_id) references role(rid)
);

CREATE TABLE purchase_category(
   cid   BIGSERIAL                   NOT NULL,
   category_name VARCHAR (20)  NOT NULL,
   description VARCHAR         NOT NULL,
   PRIMARY KEY (cid)
);

CREATE TABLE purchase(
   pid INTEGER DEFAULT NEXTVAL('purchase_id_seq'),
   purchase_name VARCHAR (20)  NOT NULL,
   category_id   INT,
   PRIMARY KEY (pid)
);

CREATE TABLE user_purchase(
   upid INTEGER DEFAULT NEXTVAL('user_purchase_id_seq'),
   user_id   INT         NOT NULL,
   purchase_id INT       NOT NULL,
   price INT             NOT NULL,
   quantity INT          NOT NULL,
   date DATE,
   PRIMARY KEY (upid),
   constraint fk_user_id foreign key(user_id) references users(uid) ON DELETE CASCADE,
   constraint fk_purchase_id foreign key(purchase_id) references purchase(pid)
);