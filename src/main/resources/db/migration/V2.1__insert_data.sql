insert into users(first_name, last_name, email)
values('Volodymyr', 'Hrebin', 'Ranga@gmail.com');
insert into users(first_name, last_name, email)
values('Olena', 'Hrebin', 'Ravi@gmail.com');

insert into account
values(-1, 'volodymyr', 'volodymyr', true, true, true, true, 1);
insert into account
values(-2, 'olena', 'olena', true, true, true, true, 2);

insert into role
values(-1, 'ROLE_ADMIN');
insert into role
values(-2, 'ROLE_USER');

insert into account_role
values(-1, -1);
insert into account_role
values(-2, -2);

insert into purchase_category
values(-1,'Food', 'foodf');
insert into purchase_category
values(-2,'Equipment', 'equipment');
insert into purchase_category
values(-3,'Stationery', 'stationery');

insert into purchase
values(-1,'Toster', -2);
insert into purchase
values(-2,'Pen', -3);
insert into purchase
values(-3,'Bread', -1);
insert into purchase
values(-4,'SSD', -2);
insert into purchase
values(-5,'Handbag', -2);

insert into user_purchase
values(-1,1,-1, 85550, 1,'2019-07-19');
insert into user_purchase
values(-2,1,-2, 3570, 1,'2019-07-19');
insert into user_purchase
values(-3,1,-3, 1545, 1,'2019-07-19');
insert into user_purchase
values(-4,1,-4, 155000, 1,'2019-07-19');
insert into user_purchase
values(-5,2,-2, 123000, 1,'2019-07-19');
insert into user_purchase
values(-6,2,-3, 123000, 1,'2019-07-19');
insert into user_purchase
values(-7,2,-5, 123000, 1,'2019-07-19');

