
CREATE TABLE user(
   uid   INT                  NOT NULL,
   user_name VARCHAR (20)     NOT NULL,
   email VARCHAR (20)         NOT NULL,
   account_id INT,
   PRIMARY KEY (uid)
);

CREATE TABLE account(
   aid   INT                  NOT NULL,
   login VARCHAR (20)     NOT NULL,
   password VARCHAR (20)         NOT NULL,
   accountNonExpired BOOLEAN  NOT NULL,
   accountNonLocked BOOLEAN  NOT NULL,
   credentialsNonExpired BOOLEAN  NOT NULL,
   enabled BOOLEAN  NOT NULL,
   user_id INT,
   PRIMARY KEY (aid)
);

CREATE TABLE role(
   rid   INT                  NOT NULL,
   name VARCHAR (20)          NOT NULL,
   PRIMARY KEY (rid)
);

CREATE TABLE account_role(
   account_id   INT         NOT NULL,
   role_id INT       NOT NULL,
   constraint fk_account_id foreign key(account_id) references account(aid),
   constraint fk_role_id foreign key(role_id) references role(rid)
);

CREATE TABLE purchase_category(
   cid   INT                   NOT NULL,
   category_name VARCHAR (20)  NOT NULL,
   description VARCHAR         NOT NULL,
   PRIMARY KEY (cid)
);

CREATE TABLE purchase(
   pid   INT                   NOT NULL,
   purchase_name VARCHAR (20)  NOT NULL,
   price INT                   NOT NULL,
   category_id   INT,
   PRIMARY KEY (pid)
);

-- ALTER TABLE
--     purchase ADD CONSTRAINT purchase_category FOREIGN KEY (pid) REFERENCES purchase_category (cid)
--     ON DELETE CASCADE
--     ON UPDATE CASCADE;

CREATE TABLE user_purchase(
   user_id   INT         NOT NULL,
   purchase_id INT       NOT NULL,
   constraint fk_user_id foreign key(user_id) references user(uid),
   constraint fk_purchase_id foreign key(purchase_id) references purchase(pid)
);